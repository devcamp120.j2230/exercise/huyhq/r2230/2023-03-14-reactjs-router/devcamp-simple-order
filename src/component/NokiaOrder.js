import { Card, CardContent, Typography, CardActions, Button, Grid } from "@mui/material";
import { useEffect, useState } from "react";
import { useLocation, useNavigate, useParams, useSearchParams } from "react-router-dom";

const NokiaOrder = (props) => {
  const { param1 } = useParams();
  const [param] = useSearchParams();
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    if (param1 === "samsung") {
      navigate("/samsung");
    }
  })

  const [phone, setPhone] = useState({ name: "Nokia 8", price: 650, quantity: 0 });
  const [quantity, setQuantity] = useState(phone.quantity);
  const onCliCkBuy = () => {
    var newArr = phone;
    newArr.quantity++;
    setPhone(newArr)
    setQuantity(newArr.quantity)
    props.getQuantity(newArr);
  };
  return (
    <Grid item xs={4} p={3}>
      <Card sx={{ minWidth: 275 }}>
        <CardContent>
          <Typography sx={{ fontSize: 14 }} color="text.primary" gutterBottom>
            {phone.name}
          </Typography>
          <Typography sx={{ fontSize: 14 }}>
            Price: {phone.price}$
          </Typography>
          <Typography sx={{ fontSize: 14 }}>
            Quantity: {quantity}
          </Typography>
        </CardContent>
        <CardActions>
          {
            location.pathname == "/"
              ? <>
                <Button size="small" variant="contained" onClick={onCliCkBuy}>Buy</Button>
                <Button size="small" variant="contained" href="/nokia">Detail</Button>
              </>
              : <>
              <Button size="small" variant="contained" href="/">Back</Button>
              </>
          }

        </CardActions>
      </Card>
    </Grid>
  )
}

export default NokiaOrder;