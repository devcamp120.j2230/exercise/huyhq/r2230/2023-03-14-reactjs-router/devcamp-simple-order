import { Grid, Typography } from "@mui/material";
import { Container } from "@mui/system"
import { useState } from "react";
import IPhoneOrder from "./IPhoneOrder";
import NokiaOrder from "./NokiaOrder";
import SamsungOrder from "./SamsungOrder";

const Order = ()=>{
    const [total, setTotal] = useState(0);

    const getQuantityIphone = (phone)=>{
        console.log(phone);
        setTotal(total + phone.price)
    }
    const getQuantitySamsung = (phone)=>{
        console.log(phone);
        setTotal(total + phone.price)
    }
    const getQuantityNokia = (phone)=>{
        console.log(phone);
        setTotal(total + phone.price)
    }

    return(
        <Container>
            <Grid container>
                <IPhoneOrder getQuantity = {getQuantityIphone}/>
                <SamsungOrder getQuantity = {getQuantitySamsung}/>
                <NokiaOrder getQuantity = {getQuantityNokia}/>
            </Grid>
            <Typography component="div">Total: {total}$</Typography>
        </Container>
    )
}

export default Order;
