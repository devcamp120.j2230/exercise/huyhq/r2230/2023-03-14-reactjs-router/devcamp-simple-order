import { Route, Routes } from "react-router-dom";
import Order from "./component/Order";
import routeList from "./routes";

function App() {

  return (
    <>
      <Routes>
        {
          routeList.map((route, index) => {
            if (route.path) {
              return <Route path={route.path} element={route.element}></Route>
            } else {
              return null
            }
          })
        }
        <Route path="*" element={<Order/>}></Route>       
        
      </Routes>

    </>
  );
}

export default App;
