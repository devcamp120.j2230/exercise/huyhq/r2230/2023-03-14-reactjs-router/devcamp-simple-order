import IPhoneOrder from "./component/IPhoneOrder";
import NokiaOrder from "./component/NokiaOrder";
import Order from "./component/Order";
import SamsungOrder from "./component/SamsungOrder";

const routeList = [
    { path:"/", element:<Order/>},
    { label:"Iphone Page", path:"/iphone", element:<IPhoneOrder/>},
    { label:"Samsung Page", path:"/samsung", element:<SamsungOrder/>},
    { label:"Nokia Page", path:"/nokia", element:<NokiaOrder/>},
    { label:"Nokia Page with Param", path:"/nokia/:param1", element:<NokiaOrder/>},

]

export default routeList;